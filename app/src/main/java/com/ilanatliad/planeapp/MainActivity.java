package com.ilanatliad.planeapp;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    public float planeGlobalSpeed = 1;
    public int planeRank = 2;
    EditText fuelLVL, propLVL, engineLVL;
    //fire base
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    protected void onStop()
    {
        String lvlOFprop = propLVL.getText().toString();
        String lvlOFengine = engineLVL.getText().toString();
        String lvlOFfuel = fuelLVL.getText().toString();
        List<String> post = new ArrayList<String>();
        post.add(lvlOFengine);
        post.add(lvlOFfuel);
        post.add(lvlOFprop);
        databaseReference.push().setValue(post);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //fire base
        propLVL = ((EditText)findViewById(R.id.propLvl));
        fuelLVL = ((EditText)findViewById(R.id.fuelLvl));
        engineLVL = ((EditText)findViewById(R.id.engineLvl));

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("EDMT_FIREBASE");

        //probably hiding the underline
        ((EditText)findViewById(R.id.money)).setKeyListener(null);
        ((EditText)findViewById(R.id.mps)).setKeyListener(null);
        ((EditText)findViewById(R.id.propLvl)).setKeyListener(null);
        ((EditText)findViewById(R.id.propPrice)).setKeyListener(null);
        ((EditText)findViewById(R.id.engineLvl)).setKeyListener(null);
        ((EditText)findViewById(R.id.enginePrice)).setKeyListener(null);
        ((EditText)findViewById(R.id.fuelLvl)).setKeyListener(null);
        ((EditText)findViewById(R.id.fuelPrice)).setKeyListener(null);
        //put the first mps value
        //calculate the money from lvls
        double fuelRank =Double.valueOf(String.valueOf(((EditText)findViewById(R.id.fuelLvl)).getText()));
        double engineRank = Double.valueOf(String.valueOf(((EditText)findViewById(R.id.engineLvl)).getText()));
        double propRank = Double.valueOf(String.valueOf(((EditText)findViewById(R.id.propLvl)).getText()));
        //plane picture name will be set by the rank it has
        ((EditText)findViewById(R.id.mps)).setText(String.valueOf(Math.pow(Math.pow(2,planeRank),fuelRank)+Math.pow(Math.pow(2,planeRank),engineRank)+Math.pow(Math.pow(2,propRank),fuelRank)));
        //get the screen size
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        final int screenHeight = size.y;


        //main timer runing whole game moving the plane changing the money
        Timer planeMoveTimer = new Timer();

        planeMoveTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                View plane = findViewById(R.id.plane);
                final double runTimeMoney = Double.parseDouble(((EditText)findViewById(R.id.money)).getText().toString());
                final double runTimeMps = Double.parseDouble(((EditText)findViewById(R.id.mps)).getText().toString());
                System.out.println("money" + runTimeMoney);
                System.out.println("mps" + runTimeMps);
                if (plane.getY() > screenHeight+90)//if its out of the screen (down)
                    plane.setY(screenHeight+90);
                if (plane.getY() < -90)//if its out of the screen (up)
                {
                    plane.setY(screenHeight+90);
                    ((EditText)findViewById(R.id.money)).setText(String.valueOf(runTimeMps+runTimeMoney));//for some reason crush   change the money value to money plus the money per second
                }
                plane.setY(plane.getY() - planeGlobalSpeed);//moving the plane
            }
        },1, 5);


        //timer that check if there are max upgrades to level up a plane and will level it up
        Timer checkPlanes = new Timer();

        checkPlanes.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                View plane = findViewById(R.id.plane);
                if (((EditText) findViewById(R.id.fuelLvl)).getText().equals("20") && ((EditText) findViewById(R.id.engineLvl)).getText().equals("20") && ((EditText) findViewById(R.id.propLvl)).getText().equals("20")) {
                    ((EditText) findViewById(R.id.fuelLvl)).setText("1");
                    ((EditText) findViewById(R.id.engineLvl)).setText("1");
                    ((EditText) findViewById(R.id.propLvl)).setText("1");
                    planeRank++;
                    switch (planeRank) {
                        case 2:
                            ((ImageView) findViewById(R.id.plane)).setImageResource(R.drawable.whiteplane);//change the image to the other one by id (get id by name)
                        case 3:
                            ((ImageView) findViewById(R.id.plane)).setImageResource(R.drawable.redwhiteplane);
                        case 4:
                            ((ImageView) findViewById(R.id.plane)).setImageResource(R.drawable.jetplane);

                    }

                }
            }
        },1, 5);


    }
    public void buttonOnClick(View v)
    {
        double money = 0;
        //change the money amount
        money = Double.parseDouble(((EditText)findViewById(R.id.mps)).getText().toString());
        money += 10;
        ((EditText)findViewById(R.id.mps)).setText(String.valueOf(money));

    }
    public  void  shopOnClick(View v)
    {
        (findViewById(R.id.popup)).setVisibility(View.VISIBLE);
        (findViewById(R.id.engine)).setVisibility(View.VISIBLE);
        (findViewById(R.id.engineLvl)).setVisibility(View.VISIBLE);
        (findViewById(R.id.enginePrice)).setVisibility(View.VISIBLE);
        (findViewById(R.id.fuel)).setVisibility(View.VISIBLE);
        (findViewById(R.id.fuelPrice)).setVisibility(View.VISIBLE);
        (findViewById(R.id.fuelLvl)).setVisibility(View.VISIBLE);
        (findViewById(R.id.propellor)).setVisibility(View.VISIBLE);
        (findViewById(R.id.propLvl)).setVisibility(View.VISIBLE);
        (findViewById(R.id.propPrice)).setVisibility(View.VISIBLE);
        (findViewById(R.id.exit)).setVisibility(View.VISIBLE);
        (findViewById(R.id.wallet)).setVisibility(View.VISIBLE);
        (findViewById(R.id.purchase)).setVisibility(View.VISIBLE);
        (findViewById(R.id.NBengineLvl)).setVisibility(View.VISIBLE);
    }
    public  void  exitOnClick(View v)
    {
        (findViewById(R.id.popup)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.engine)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.engineLvl)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.enginePrice)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.fuel)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.fuelPrice)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.fuelLvl)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.propellor)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.propLvl)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.propPrice)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.exit)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.wallet)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.purchase)).setVisibility(View.INVISIBLE);
        (findViewById(R.id.NBengineLvl)).setVisibility(View.INVISIBLE);


    }
}
